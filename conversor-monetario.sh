#!/bin/bash
set -e

if [ "$#" -ne 3 ]; then 
	echo "favor informar 3 parametros moeda origem destino e valor desejado" 
	exit 1
fi

echo $1
echo $2
echo $3

multiplicador=`curl 'https://api.exchangeratesapi.io/latest?base='$1'&symbols='$2 | jq '.rates.'"$2"` 

python -c "print ${multiplicador} * $3"
